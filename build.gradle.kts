import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.3.2.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.3.72"
    kotlin("plugin.spring") version "1.3.72"
    kotlin("plugin.jpa") version "1.3.72"

    // It is a necessary plug-in for an optimized Docker image configuration.
    // Reference Link: https://github.com/palantir/gradle-docker
    id("com.palantir.docker") version "0.22.1"
}

group = "ai.maum.auth"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
    google()
    jcenter()
    maven(url = "http://maven.jahia.org/maven2")
}

extra["springCloudVersion"] = "Hoxton.SR7"

dependencies {
    // [Default] When spring boot with kotlin
    implementation("org.springframework.boot:spring-boot-starter-web")
    // Define over spring boot 2.3
    implementation("org.springframework.boot:spring-boot-starter-validation")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // [Logging]
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("com.lmax:disruptor:3.4.2")

    // [DataBase]
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation ("com.oracle:ojdbc6:11.2.0.3")
    runtimeOnly("com.h2database:h2")

    // [Security]
    implementation("org.springframework.cloud:spring-cloud-starter-security")
    implementation("org.springframework.cloud:spring-cloud-starter-oauth2")

    // [Cache]
    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("org.ehcache:ehcache")
    implementation("javax.cache:cache-api")

    // [Test]
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.springframework.security:spring-security-test")
}

configurations {
    all {
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    }
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.create<Copy>("unpack") {
    dependsOn(tasks.bootJar)
    from(zipTree(tasks.bootJar.get().outputs.files.singleFile))
    into("build/dependency")
}

docker {
    name = "maum-auth"
    copySpec.from(tasks.findByName("unpack")?.outputs).into("dependency")
    buildArgs(mapOf("DEPENDENCY" to "dependency"))
}
