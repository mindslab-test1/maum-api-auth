-- inert into ROLE ENTITY
insert into API_AUTH.ROLE_ENTITY (ID, CREATED_AT, UPDATED_AT, ROLE_NAME) values (ROLE_SEQ.nextval, current_timestamp, current_timestamp, 'ROLE_API');
insert into API_AUTH.ROLE_ENTITY (ID, CREATED_AT, UPDATED_AT, ROLE_NAME) values (ROLE_SEQ.nextval, current_timestamp, current_timestamp, 'ROLE_CLOUD');
insert into API_AUTH.ROLE_ENTITY (ID, CREATED_AT, UPDATED_AT, ROLE_NAME) values (ROLE_SEQ.nextval, current_timestamp, current_timestamp, 'ROLE_API_ADMIN');

-- insert into SCOPE ENTITY
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-api-use-*', 1);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-cloud-*', 2);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-cloud-role-*', 2);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-cloud-scope-*', 2);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-cloud-account-*', 2);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-cloud-application-*', 2);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-cloud-app_key-*', 2);
insert into API_AUTH.SCOPE_ENTITY (ID, CREATED_AT, UPDATED_AT, SCOPE_NAME, ROLE_ID) values (SCOPE_SEQ.nextval, current_timestamp, current_timestamp, 'ai-maum-api-admin-*', 3);

-- create account
insert into ACCOUNT_ENTITY values (ACCOUNT_SEQ.nextval, current_timestamp, current_timestamp, 'demo@mindslab.ai', 0,  'NOT_PROVIDED', 'demo');

-- create application
insert into APPLICATION_ENTITY values (APP_SEQ.nextval, current_timestamp, current_timestamp, 'demo-application', 0, 1);

-- insert application roles
insert into APP_ROLES values (1, 1);
insert into APP_ROLES values (1, 2);
insert into APP_ROLES values (1, 3);

-- create app_sign_key
insert into APP_SIGN_KEY_ENTITY values (APP_REF_SEQ.nextval, current_timestamp, current_timestamp, 1209600, '1ea51edf-fe6c-4627-8f0a-78f03df28f39', '{noop}ac2cab41-75f3-462a-a47d-9fcc450cc5bc', 0, 2592000, 1, 1);

-- create app_sign_key_scope
insert into APP_KEY_SCOPES values (1, 1);
insert into APP_KEY_SCOPES values (1, 2);
insert into APP_KEY_SCOPES values (1, 3);
insert into APP_KEY_SCOPES values (1, 4);
insert into APP_KEY_SCOPES values (1, 5);
insert into APP_KEY_SCOPES values (1, 6);
insert into APP_KEY_SCOPES values (1, 7);
insert into APP_KEY_SCOPES values (1, 8);