package ai.maum.auth.infra.repository

import ai.maum.auth.infra.entity.AppSignKeyEntity
import org.springframework.data.repository.CrudRepository

interface AppSignKeyRepository: CrudRepository<AppSignKeyEntity, Long> {
    fun findByClientKey(clientKey: String): AppSignKeyEntity?
}