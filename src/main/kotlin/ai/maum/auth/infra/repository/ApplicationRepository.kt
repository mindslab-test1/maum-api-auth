package ai.maum.auth.infra.repository

import ai.maum.auth.infra.entity.ApplicationEntity
import org.springframework.data.repository.CrudRepository

interface ApplicationRepository: CrudRepository<ApplicationEntity, Long> {
    fun findByApplicationName(applicationName: String): ApplicationEntity?
}