package ai.maum.auth.infra.config


import ai.maum.auth.infra.repository.AppSignKeyRepository
import ai.maum.auth.infra.service.MaumAuthTokenEnhancer
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore


@Configuration
@EnableAuthorizationServer
class OAuth2AuthorizationServerConfig (
        private val clientDetailsService: ClientDetailsService,
        @Qualifier("authenticationManagerBean")
        private val authenticationManager: AuthenticationManager,
        private val jwtSignKeyConfig: JwtSignKeyConfig,
        private val appSignKeyRepository: AppSignKeyRepository
) : AuthorizationServerConfigurerAdapter() {

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.withClientDetails(clientDetailsService)
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {

        endpoints
                .pathMapping("/oauth/token", "/auth/oauth/token")
                .tokenEnhancer(maumTokenEnhancerChain())
//                .accessTokenConverter(jwtAccessTokenConverter())
                .authenticationManager(authenticationManager)
    }

    @Bean
    fun tokenStore(): JwtTokenStore {
        return JwtTokenStore(jwtAccessTokenConverter())
    }

    @Bean
    fun jwtAccessTokenConverter(): JwtAccessTokenConverter {
        val accessTokenConverter = JwtAccessTokenConverter()
        accessTokenConverter.setKeyPair(jwtSignKeyConfig.generateKeyPair())
        return accessTokenConverter
    }

    @Bean
    fun maumAuthTokenEnhancer(): TokenEnhancer {
        return MaumAuthTokenEnhancer(appSignKeyRepository)
    }

    @Bean
    fun maumTokenEnhancerChain(): TokenEnhancerChain {
        val tokenEnhancerChain = TokenEnhancerChain()
        tokenEnhancerChain.setTokenEnhancers(
                listOf(maumAuthTokenEnhancer(), jwtAccessTokenConverter())
        )
        return tokenEnhancerChain
    }
}