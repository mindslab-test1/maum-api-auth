package ai.maum.auth.infra.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import java.security.*
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*

@Configuration
class JwtSignKeyConfig(
        @Value("\${jwt.public-key}")
        private val jwtPublicKey: String,
        @Value("\${jwt.private-key}")
        private val jwtPrivateKey: String,
        @Value("\${jwt.sign-key:maum-api-auth-signkey}")
        private val jwtSignKey: String
) {

    fun generateKeyPair(): KeyPair {
        return KeyPair(getPublicKey(), getPrivateKey())
    }

    private fun getKeyFactoryWithRSA(): KeyFactory {
        return try {
            KeyFactory.getInstance("RSA")
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        }
    }

    private fun getPrivateKey(): PrivateKey? {
        return try {
            val privateKey = jwtPrivateKey.replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replace("\\s+".toRegex(), "")
            val pkcs8EncodedKeySpec = PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey))
            getKeyFactoryWithRSA().generatePrivate(pkcs8EncodedKeySpec)
        } catch (e: InvalidKeySpecException) {
            throw RuntimeException(e)
        }
    }

    private fun getPublicKey(): PublicKey? {
        return try {
            val publicKey = jwtPublicKey.replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace("-----END PUBLIC KEY-----", "")
                    .replace("\\s+".toRegex(), "")
            val x509EncodedKeySpec = X509EncodedKeySpec(Base64.getDecoder().decode(publicKey))
            getKeyFactoryWithRSA().generatePublic(x509EncodedKeySpec)
        } catch (e: InvalidKeySpecException) {
            throw RuntimeException(e)
        }
    }
}