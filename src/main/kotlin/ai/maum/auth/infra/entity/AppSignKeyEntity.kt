package ai.maum.auth.infra.entity

import ai.maum.auth.core.model.AppSignKey
import ai.maum.auth.core.model.AuthorizedGrantType
import org.hibernate.annotations.DynamicUpdate
import java.time.ZoneId
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "APP_SIGN_KEY_ENTITY")
class AppSignKeyEntity: BaseEntity() {
    @Id
    @SequenceGenerator(name = "APP_REF_SEQ_GEN", sequenceName = "APP_REF_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_REF_SEQ_GEN")
    var id: Long? = null

    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    var application: ApplicationEntity? = null

    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinColumn(name = "key_owner_id", referencedColumnName = "id")
    var keyOwner: AccountEntity? = null

    @Column(unique = true)
    var clientKey: String = ""

    @Column(unique = true)
    var clientSecret: String = ""

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "APP_KEY_SCOPES",
            joinColumns = [JoinColumn(name = "sign_key_id")],
            inverseJoinColumns = [JoinColumn(name = "scope_id")]
    )
    var scopes: Set<ScopeEntity>? = null

    @Column
    var accessTokenValiditySeconds: Int? = null

    @Column
    var refreshTokenValiditySeconds: Int? = null

    @Column
    var locked: Boolean = false
}

fun AppSignKeyEntity.toModel(): AppSignKey{
    val scopeSet = mutableSetOf<String>()
    this.scopes?.forEach {scope ->
        scopeSet.add(scope.scopeName!!)
    }

    val roleSet = mutableSetOf<String>()
    this.application!!.roles!!.forEach { role ->
        roleSet.add(role.roleName!!)
    }

    return AppSignKey(
            applicationName = this.application!!.applicationName,
            scope = scopeSet,
            resourceIds = mutableListOf(),
            clientId = this.clientKey,
            clientSecret = this.clientSecret,
            authorities = roleSet,
            accessTokenValiditySeconds = this.accessTokenValiditySeconds,
            refreshTokenValiditySeconds = this.refreshTokenValiditySeconds,
            createdAt = this.createdAt?.atZone(ZoneId.systemDefault()),
            updatedAt = this.updatedAt?.atZone(ZoneId.systemDefault()),
            authorizedGrantTypes = setOf(AuthorizedGrantType.CLIENT_CREDENTIALS.value)
    )
}