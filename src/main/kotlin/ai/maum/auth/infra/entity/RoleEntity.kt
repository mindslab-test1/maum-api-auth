package ai.maum.auth.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "ROLE_ENTITY")
@DynamicUpdate
class RoleEntity: BaseEntity() {
    @Id
    @SequenceGenerator(name = "ROLE_SEQ_GEN", sequenceName = "ROLE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var roleName: String = ""

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    var validScopes: Set<ScopeEntity>? = null

    @ManyToMany(mappedBy = "roles")
    var apps: Set<ApplicationEntity>? = null
}