package ai.maum.auth.infra.service

import ai.maum.auth.core.exception.InvalidClientIdException
import ai.maum.auth.core.exception.NotFoundClientException
import ai.maum.auth.infra.entity.toModel
import ai.maum.auth.infra.repository.AppSignKeyRepository
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Primary
import org.springframework.security.oauth2.provider.ClientDetails
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.client.BaseClientDetails
import org.springframework.stereotype.Repository

@Primary
@Repository
class AppSignKeyValidateService(
        private val appSignKeyRepository: AppSignKeyRepository
) : ClientDetailsService{
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun loadClientByClientId(clientId: String): ClientDetails {
        logger.info("load app sign key($clientId)")
        val appSignKeyEntity = appSignKeyRepository.findByClientKey(clientId)
                ?: throw NotFoundClientException("Not found clientId($clientId)")
        logger.debug("check locks")
        if(
                appSignKeyEntity.locked ||
                appSignKeyEntity.application?.locked == true ||
                appSignKeyEntity.application?.ownerAccount?.locked == true ||
                appSignKeyEntity.keyOwner?.locked == true

        )
            throw InvalidClientIdException("locked keypair")

        logger.debug("get scopes")
        val scopeSetAll = mutableSetOf<String>()
        appSignKeyEntity.scopes?.forEach { scopeEntity ->
            scopeEntity.scopeName?.let { scopeSetAll.add(it) }
        }
        logger.debug("scope set stated: $scopeSetAll")

        val scopeSetAvail = mutableSetOf<String>()
        appSignKeyEntity.application?.roles?.forEach { roleEntity ->
            roleEntity.validScopes?.forEach {  scopeEntity ->
                scopeEntity.scopeName?.let { scopeSetAvail.add(it)}
            }
        }
        logger.debug("scope set available: $scopeSetAvail")

        val scopeSetValid = scopeSetAvail.intersect(scopeSetAll)
        logger.debug("scope set valid: $scopeSetValid")

        val appSignKeyModel = appSignKeyEntity.toModel()
        if(scopeSetValid.isEmpty()) throw InvalidClientIdException("no scope available")
        appSignKeyModel.scope = scopeSetValid

        val appSignKeyDetails = BaseClientDetails(appSignKeyModel)
        logger.debug(appSignKeyDetails.toString())

        return appSignKeyDetails
    }
}