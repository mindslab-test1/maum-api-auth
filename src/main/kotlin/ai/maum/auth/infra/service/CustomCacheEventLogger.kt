package ai.maum.auth.infra.service

import org.ehcache.event.CacheEvent
import org.ehcache.event.CacheEventListener
import org.slf4j.LoggerFactory

class CustomCacheEventLogger : CacheEventListener<Any, Any> {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun onEvent(event: CacheEvent<out Any, out Any>?) {
        logger.info("Cache event = ${event?.type}, Key = ${event?.key}, Old value = ${event?.oldValue}, New value = ${event?.newValue}")
    }
}