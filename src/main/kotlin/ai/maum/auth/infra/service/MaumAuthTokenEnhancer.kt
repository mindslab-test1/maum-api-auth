package ai.maum.auth.infra.service

import ai.maum.auth.core.exception.NotFoundClientException
import ai.maum.auth.infra.repository.AppSignKeyRepository
import ai.maum.auth.utils.Constants
import org.slf4j.LoggerFactory
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.TokenEnhancer

class MaumAuthTokenEnhancer(
        private val appSignKeyRepository: AppSignKeyRepository
): TokenEnhancer {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun enhance(accessToken: OAuth2AccessToken, authentication: OAuth2Authentication?): OAuth2AccessToken {
        val additionalInfo = mutableMapOf<String, Any>()
        val appSignKey = appSignKeyRepository.findByClientKey(authentication?.principal.toString())
                ?: throw NotFoundClientException("no app key found!")
        val applicationInfo = appSignKey.application
                ?: throw NotFoundClientException("no parent application found")

        additionalInfo[Constants.JWT_ADDITIONAL_KEY_APP_NAME] = applicationInfo.applicationName
        additionalInfo[Constants.JWT_ADDITIONAL_KEY_APP_OWNER] = applicationInfo.ownerAccount?.id ?: -1
        additionalInfo[Constants.JWT_ADDITIONAL_KEY_KEY_OWNER] = appSignKey.keyOwner?.id ?: -1
        (accessToken as DefaultOAuth2AccessToken).additionalInformation = additionalInfo

        return accessToken
    }
}