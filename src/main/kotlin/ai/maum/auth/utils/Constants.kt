package ai.maum.auth.utils

object Constants {
    // [Encryption Info]
    const val PASSWORD_NOOP = "{noop}"

    // [additional info key]
    const val JWT_ADDITIONAL_KEY_APP_NAME = "app_id"
    const val JWT_ADDITIONAL_KEY_APP_OWNER = "app_owner"
    const val JWT_ADDITIONAL_KEY_KEY_OWNER = "key_owner"

    // [Cache Info]
    const val CLIENT_ID_CACHE_NAME = "clientIdCache"

    // [Exception Info]
    // exception fields
    const val ERROR_CODE = "code"
    const val ERROR_MESSAGE = "message"

    // authentication error
    const val UNAUTHORIZED_CODE = "unauthorized"
    const val UNAUTHORIZED_MESSAGE = "Invalid clientId or clientSecret"

    // access denied error
    const val ACCESS_DENIED_CODE = "access_denied"
    const val ACCESS_DENIED_MESSAGE = "Please check your authorization"
}
