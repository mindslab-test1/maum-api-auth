package ai.maum.auth.utils

object ErrorCode {
    const val EXIST_ACCOUNT_EMAIL = "exist_account_email"
    const val EXIST_CLIENT_APPLICATION_NAME = "exist_client_application_name"
    const val INVALID_CLIENT_ID = "invalid_client_id"
    const val INVALID_CLIENT_SECRET = "invalid_client_secret"
    const val NOT_FOUND_ACCOUNT = "not_fount_account"
    const val NOT_FOUND_CLIENT = "not_fount_client"
}
