package ai.maum.auth.boundary


import ai.maum.auth.core.exception.BaseException
import ai.maum.auth.utils.Constants
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(RuntimeException::class)
    fun handle(exception: RuntimeException, request: WebRequest): ResponseEntity<Any?>? {
        logger.warn("handleBaseException", exception)
        val status = when (exception) {
            is BaseException -> HttpStatus.BAD_REQUEST
            else -> HttpStatus.INTERNAL_SERVER_ERROR
        }

        if (status == HttpStatus.INTERNAL_SERVER_ERROR) {
            logger.error("internal_error", exception)
        }

        val code = when (exception) {
            is BaseException -> exception.code
            else -> "internal_server_error"
        }

        val responseBody = mapOf(Constants.ERROR_CODE to code, Constants.ERROR_MESSAGE to exception.message)

        return handleExceptionInternal(
            exception,
            responseBody,
            HttpHeaders(),
            status,
            request
        )
    }

    override fun handleBindException(
        ex: BindException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        logger.info("handleBindException")
        return super.handleBindException(ex, headers, status, request)
    }
}
