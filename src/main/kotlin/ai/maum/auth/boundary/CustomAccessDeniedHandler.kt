package ai.maum.auth.boundary

import ai.maum.auth.utils.Constants
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class CustomAccessDeniedHandler : AccessDeniedHandler {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun handle(request: HttpServletRequest, response: HttpServletResponse, accessDeniedException: AccessDeniedException) {
        // status를 403 에러로 지정
        response.status = HttpServletResponse.SC_FORBIDDEN

        response.contentType = MediaType.APPLICATION_JSON_VALUE
        val objectMapper = ObjectMapper()
        val code = Constants.ACCESS_DENIED_CODE
        val message = Constants.ACCESS_DENIED_MESSAGE

        val responseBody = mapOf(Constants.ERROR_CODE to code, Constants.ERROR_MESSAGE to message)

        val jsonBody = objectMapper.writeValueAsString(responseBody)

        val out = response.writer
        out.print(jsonBody)
        out.flush()
    }
}