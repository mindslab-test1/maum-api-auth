package ai.maum.auth.boundary

import ai.maum.auth.utils.Constants
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class CustomAuthenticationEntryPoint : AuthenticationEntryPoint {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, authException: AuthenticationException) {
        logger.info("custom authentication entry point")

        // status를 401 에러로 지정
        response.status = HttpServletResponse.SC_UNAUTHORIZED

        response.contentType = "${MediaType.APPLICATION_JSON_VALUE};charset=${StandardCharsets.UTF_8.name()}"

        val objectMapper = ObjectMapper()
        val code = Constants.UNAUTHORIZED_CODE
        val message = Constants.UNAUTHORIZED_MESSAGE

        val responseBody = mapOf(Constants.ERROR_CODE to code, Constants.ERROR_MESSAGE to message)

        val jsonBody = objectMapper.writeValueAsString(responseBody)

        val out = response.writer
        out.print(jsonBody)
        out.flush()
    }
}
