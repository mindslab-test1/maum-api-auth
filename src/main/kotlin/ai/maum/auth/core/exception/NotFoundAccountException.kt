package ai.maum.auth.core.exception

import ai.maum.auth.utils.ErrorCode
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class NotFoundAccountException (
        override val message: String
) : BaseException(ErrorCode.NOT_FOUND_ACCOUNT, message)
