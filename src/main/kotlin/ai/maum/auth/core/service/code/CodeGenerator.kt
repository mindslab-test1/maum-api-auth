package ai.maum.auth.core.service.code

interface CodeGenerator {
    fun generate(len: Int = 12): String
}