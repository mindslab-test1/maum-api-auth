package ai.maum.auth.core.service.code

import org.springframework.stereotype.Service
import kotlin.streams.asSequence

@Service
class DefaultCodeGenerator : CodeGenerator {
    override fun generate(len: Int): String {
        val source = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return java.util.Random().ints(len.toLong(), 0, source.length)
                .asSequence()
                .map(source::get)
                .joinToString("")
    }
}