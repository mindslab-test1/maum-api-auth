package ai.maum.auth.core.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.oauth2.provider.ClientDetails
import java.time.ZonedDateTime

data class AppSignKey(
        val applicationName: String = "",
        var scope: Collection<String>,
        val resourceIds: Collection<String>,
        private val clientId: String = "",
        private val clientSecret: String = "",
        private val authorities: Collection<String>,
        private val accessTokenValiditySeconds: Int?,
        private val refreshTokenValiditySeconds: Int?,
        val authorizedGrantTypes: Collection<String>,
        val createdAt: ZonedDateTime? = null,
        val updatedAt: ZonedDateTime? = null

): ClientDetails {
    override fun isSecretRequired(): Boolean {
        return clientSecret.isNotEmpty()
    }

    override fun getAdditionalInformation(): MutableMap<String, Any> {
        return mutableMapOf()
    }

    override fun getAccessTokenValiditySeconds(): Int {
        return accessTokenValiditySeconds ?: 0
    }

    override fun getResourceIds(): MutableSet<String> {
        return resourceIds.toHashSet()
    }

    override fun getClientId(): String {
        return clientId
    }

    override fun isAutoApprove(scope: String?): Boolean {
        return true
    }

    override fun getAuthorities(): MutableCollection<GrantedAuthority> {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(authorities.toSet().joinToString(","))
    }

    override fun getRefreshTokenValiditySeconds(): Int {
        return refreshTokenValiditySeconds ?: 0
    }

    override fun getClientSecret(): String {
        return clientSecret
    }

    override fun getRegisteredRedirectUri(): MutableSet<String> {
        return setOf("").toHashSet()
    }

    override fun isScoped(): Boolean {
        return scope.toHashSet().isNotEmpty()
    }

    override fun getScope(): MutableSet<String> {
        return scope.toHashSet()
    }

    override fun getAuthorizedGrantTypes(): MutableSet<String> {
        return authorizedGrantTypes.toHashSet()
    }
}