package ai.maum.auth.core.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

enum class AuthorizedGrantType(@get:JsonValue val value: String) {
    AUTHORIZED_CODE("authorized_code"),
    IMPLICIT("implicit"),
    CLIENT_CREDENTIALS("client_credentials"),
    PASSWORD("password");

    companion object {
        private val map = values().associateBy(AuthorizedGrantType::value)

        @JsonCreator
        @JvmStatic
        fun fromValue(value: String) = map.getOrDefault(value, CLIENT_CREDENTIALS)
    }
}