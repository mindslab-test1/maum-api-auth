package ai.maum.auth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MaumApiAuthApplication

fun main(args: Array<String>) {
    runApplication<MaumApiAuthApplication>(*args)
}
